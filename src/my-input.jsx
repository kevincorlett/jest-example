import React from 'react';
import ReactDOM from 'react-dom';


class MyInput extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            value: props.value
        }

    }

    render() {
        return (
            <input value={this.state.value} onChange={this.onChange} />
        );
    }

    onChange(){
        console.log('do something');
    }
}

export default MyInput;
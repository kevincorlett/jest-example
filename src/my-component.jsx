import React from 'react';
import ReactDOM from 'react-dom';

import MyInput from './my-input.jsx';

class MyComponent extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            value: props.someValue
        }

    }

    render() {
        return (
            <MyInput value={this.state.value} />
        );
    }
}

export default MyComponent;
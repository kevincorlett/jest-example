import React from 'react';
import ReactDOM from 'react-dom';

import MyComponent from './my-component';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() })

describe('my-component', () => {
    it('supplies the value to the input box', () => {
        const fakeProps = { someValue: 'something' };

        var target = mount(<MyComponent {...fakeProps} />);

        const theInput = target.find('MyInput').get(0);

        expect(theInput.props.value).toBe(fakeProps.someValue);

    });
});